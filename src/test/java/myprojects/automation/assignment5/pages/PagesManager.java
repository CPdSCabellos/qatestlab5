package myprojects.automation.assignment5.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PagesManager {

    private MainPage mainPage;
    private AllProductsPage allProductsPage;
    private ProductPage productPage;
    private ConfirmPage confirmPage;
    private BasketPage basketPage;
    private UserInfoPage userInfoPage;
    private OrderConfirmPage orderConfirmPage;

    public PagesManager(WebDriver driver, WebDriverWait wait) {
        mainPage = new MainPage(driver, wait);
        allProductsPage = new AllProductsPage(driver, wait);
        productPage = new ProductPage(driver, wait);
        confirmPage = new ConfirmPage(driver, wait);
        basketPage = new BasketPage(driver, wait);
        userInfoPage = new UserInfoPage(driver, wait);
        orderConfirmPage = new OrderConfirmPage(driver, wait);
    }


    public MainPage getMainPage() {
        return mainPage;
    }

    public AllProductsPage getAllProductsPage() {
        return allProductsPage;
    }

    public ProductPage getProductPage() {
        return productPage;
    }

    public ConfirmPage getConfirmPage() {
        return confirmPage;
    }

    public BasketPage getBasketPage() {
        return basketPage;
    }

    public UserInfoPage getUserInfoPage() {
        return userInfoPage;
    }

    public OrderConfirmPage getOrderConfirmPage() {
        return orderConfirmPage;
    }
}
