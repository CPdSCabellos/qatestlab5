package myprojects.automation.assignment5.pages;

import myprojects.automation.assignment5.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends Page{

    MainPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {

    }

    public void openPage() {
        getDriver().navigate().to(Properties.getBaseUrl());
    }

    public void showAllProduct(){
        WebElement allProductsBtn = scrollTo(By.className("all-product-link"));
        allProductsBtn.click();
    }
}
