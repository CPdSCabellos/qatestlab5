package myprojects.automation.assignment5.pages;

import myprojects.automation.assignment5.model.UserData;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UserInfoPage extends Page {
    private final By guestFormLocator = By.id("checkout-guest-form");
    private final By customerFormLocator = By.id("customer-form");
    private final By firstNameLocator = By.name("firstname");
    private final By lastNameLocator = By.name("lastname");
    private final By emailLocator = By.name("email");
    private final By continueBtnLocator = By.name("continue");

    private final By addressFormLocator = By.id("delivery-address");
    private final By addressFieldLocator = By.name("address1");
    private final By postCodeLocator = By.name("postcode");
    private final By cityLocator = By.name("city");
    private final By addressFormFooterLocator = By.className("form-footer");

    private final By btnLocator = By.className("btn");

    private final By deliveryFormLocator = By.id("checkout-delivery-step");
    private final By continueAfterDeliveryBtnLocator = By.name("confirmDeliveryOption");

    private final By paymentFormLocator = By.id("checkout-payment-step");
    private final By paymentCheckBoxLocator = By.id("payment-option-1");
    private final By paymentApproveLocator = By.name("conditions_to_approve[terms-and-conditions]");
    private final By paymentConfirmationLocator = By.id("payment-confirmation");

    UserInfoPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {
        getWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(guestFormLocator));
    }

    public void fillUserInfo(UserData user){
        WebElement formElement = getDriver().findElement(customerFormLocator);

        WebElement userNameInput = formElement.findElement(firstNameLocator);
        setDataToInput(userNameInput, user.getName());

        WebElement lastNameInput = formElement.findElement(lastNameLocator);
        setDataToInput(lastNameInput, user.getLastName());

        WebElement emailInput = formElement.findElement(emailLocator);
        setDataToInput(emailInput, user.getEmail());

        WebElement continueBtn = formElement.findElement(continueBtnLocator);
        continueBtn.submit();
    }

    public void fillAddressInfo(UserData user){
        WebElement addressCheckForm = getDriver().findElement(addressFormLocator);
        getWait().until(ExpectedConditions.visibilityOf(addressCheckForm));

        WebElement fieldsElement = addressCheckForm.findElement(By.className("form-fields"));
        WebElement addressField = fieldsElement.findElement(addressFieldLocator);
        setDataToInput(addressField, user.getAddress());

        WebElement postCode = addressCheckForm.findElement(postCodeLocator);
        setDataToInput(postCode, String.valueOf(user.getPostCode()));

        WebElement city = addressCheckForm.findElement(cityLocator);
        setDataToInput(city, user.getCity());

        WebElement formFooter = addressCheckForm.findElement(addressFormFooterLocator);
        WebElement continueBtn = formFooter.findElement(btnLocator);
        continueBtn.click();
    }

    public void confirmDelivery(){
        WebElement deliveryForm = getDriver().findElement(deliveryFormLocator);
        getWait().until(ExpectedConditions.visibilityOf(deliveryForm));
        WebElement jsDeliveryElement = getDriver().findElement(By.id("js-delivery"));
        WebElement continueBtn = jsDeliveryElement.findElement(continueAfterDeliveryBtnLocator);
        continueBtn.click();
    }

    public void checkPayment(){
        getWait().until(ExpectedConditions.visibilityOfElementLocated(paymentConfirmationLocator));
        WebElement paymentForm = getDriver().findElement(paymentFormLocator);
        WebElement checkElement = paymentForm.findElement(paymentCheckBoxLocator);
        checkElement.click();

        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id("payment-option-1-additional-information")));
        WebElement approveElement = paymentForm.findElement(paymentApproveLocator);
        approveElement.click();

        WebElement paymentConfirmation = paymentForm.findElement(paymentConfirmationLocator);
        getWait().until(webDriver -> paymentForm.findElement(btnLocator).isEnabled());
        WebElement orderBtn = paymentConfirmation.findElement(btnLocator);
        orderBtn.click();
    }

    private void setDataToInput(WebElement input, String data){
        input.sendKeys(Keys.BACK_SPACE);
        input.sendKeys(data);
    }
}
