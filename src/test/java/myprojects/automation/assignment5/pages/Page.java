package myprojects.automation.assignment5.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {

    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor js;

    Page(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        js = (JavascriptExecutor)driver;
    }

    public abstract void waitPageLoading();

    protected WebDriver getDriver() {
        return driver;
    }

    protected WebDriverWait getWait() {
        return wait;
    }


    WebElement scrollTo(By locator){
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        return scrollTo(element);
    }

    WebElement scrollTo(WebElement element){
        js.executeScript("arguments[0].scrollIntoView(true);",element);
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }
}
