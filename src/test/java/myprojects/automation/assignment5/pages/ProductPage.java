package myprojects.automation.assignment5.pages;

import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class ProductPage extends Page {

    private final By productNameLocator = By.className("h1");
    private final By productPriceLocator = By.className("current-price");
    private final By productDetailsLocator = By.id("product-details");
    private final By qtyLocator = By.className("product-quantities");

    private final By infoTabs =By.className("nav-tabs");
    private final By addToCardLocator = By.className("product-add-to-cart");
    private final By productQtyLocator = By.className("product-quantity");
    private final By addLocator =By.className("add");

    ProductPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {
        getWait().until(ExpectedConditions.presenceOfElementLocated(productPriceLocator));
    }

    public ProductData getProductDataInfo(){

        WebElement nameElement = getDriver().findElement(productNameLocator);
        String productName = nameElement.getText();

        WebElement priceElement = getDriver().findElement(productPriceLocator);
        WebElement priceSpanElement = priceElement.findElement(By.xpath("./span"));
        String priceString = priceSpanElement.getText();
        float price = DataConverter.parsePriceValue(priceString);

        WebElement navTabMenu = scrollTo(infoTabs);
        List<WebElement> tabs = navTabMenu.findElements(By.className("nav-link"));
        WebElement productDetails = null;
        for (WebElement tabElement
                :tabs){
            String hrefInfo = tabElement.getAttribute("href");
            if(hrefInfo.contains("product-details")){
                productDetails = tabElement;
            }
        }

        Assert.assertNotNull(productDetails);

        productDetails.click();

        getWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(qtyLocator));

        WebElement tempProductElement = getDriver().findElement(productDetailsLocator);
        WebElement qtyElement = tempProductElement.findElement(qtyLocator);
        WebElement qtySpanElement = qtyElement.findElement(By.xpath("./span"));
        String qtyString = qtySpanElement.getText();
        int qty = DataConverter.parseStockValue(qtyString);

        return new ProductData(productName, qty, price);
    }

    public void addToBasket(){
        WebElement addToCardForm = scrollTo(addToCardLocator);
        WebElement productQty = addToCardForm.findElement(productQtyLocator);
        WebElement addElement = productQty.findElement(addLocator);
        WebElement basketBtn = addElement.findElement(By.className("btn"));
        basketBtn.click();
    }
}
