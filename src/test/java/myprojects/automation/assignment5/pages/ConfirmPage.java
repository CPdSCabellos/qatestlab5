package myprojects.automation.assignment5.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class ConfirmPage extends Page {

    private final By modalTitleLocator = By.className("modal-title");

    private final By modalCardLocator = By.id("blockcart-modal");
    private final By cardContentLocator = By.className("cart-content");
    private final By bodyRowLocator = By.className("row");
    private final By bodyLocator = By.className("modal-body");
    private final By modalContentLocator = By.className("modal-content");
    private final By modalDialogLocator = By.className("modal-dialog");

    ConfirmPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {
        getWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(modalTitleLocator));
    }

    public void startOrder(){
        WebElement modalElement = getDriver().findElement(modalCardLocator);
        WebElement modalDialogElement = modalElement.findElement(modalDialogLocator);
        WebElement modalContentElement = modalDialogElement.findElement(modalContentLocator);
        WebElement modalBodyElement = modalContentElement.findElement(bodyLocator);
        WebElement modalRowElement = modalBodyElement.findElement(bodyRowLocator);
        WebElement tempBlock = null;
        List<WebElement> elements = modalRowElement.findElements(By.xpath("./div"));
        for (WebElement element :
                elements) {
            String sClass = element.getAttribute("class");
            if(sClass.equals("col-md-6")){
                tempBlock = element;
            }
        }

        Assert.assertNotNull(tempBlock);
        WebElement modalContent = tempBlock.findElement(cardContentLocator);
        WebElement startOrderBtn = modalContent.findElement(By.xpath("./a"));
        startOrderBtn.click();
    }

}
