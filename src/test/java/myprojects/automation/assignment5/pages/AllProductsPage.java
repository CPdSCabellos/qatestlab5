package myprojects.automation.assignment5.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class AllProductsPage extends Page {

    private final By tableLocator = By.className("product-miniature");
    private final By sortLocator = By.className("sort-by-row");
    private final By productContainerLocator = By.className("thumbnail-container");
    private final By productDescriptionLocator = By.className("product-description");
    private final By productTitleLocator = By.className("product-title");

    AllProductsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {
        getWait().until(ExpectedConditions.presenceOfElementLocated(sortLocator));
    }

    public int showRandomProduct(){
        List<WebElement> products = getAllProductsFromPage();
        Random random = new Random();
        int productIndex = random.nextInt(products.size()-1);
        WebElement chosenProduct = products.get(productIndex);
        showMoreInfoAbout(chosenProduct);
        return productIndex;
    }

    public List<WebElement> getAllProductsFromPage(){
        return getDriver().findElements(tableLocator);
    }

    private void showMoreInfoAbout(WebElement element){
        if(!element.isDisplayed()){
            scrollTo(element);
        }
        WebElement container = element.findElement(productContainerLocator);
        WebElement productDescription = container.findElement(productDescriptionLocator);
        WebElement productTitle = productDescription.findElement(productTitleLocator);
        productTitle.click();
    }

    public void showProduct(int index){
        List<WebElement> allProducts = getAllProductsFromPage();
        showMoreInfoAbout(allProducts.get(index));
    }
}
