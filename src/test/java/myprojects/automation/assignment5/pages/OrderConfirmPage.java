package myprojects.automation.assignment5.pages;

import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class OrderConfirmPage extends Page {
    private static final String expectedMessage = "Ваш заказ подтверждён";

    private final By orderConfirmationLocator = By.id("order-confirmation");
    private final By contentHookLocator = By.id("content-hook_order_confirmation");
    private final By cardTitleLocator = By.className("card-title");

    private final By orderItemsLocator = By.id("order-items");
    private final By orderLineLocator = By.className("order-line");
    private final By orderDetailsLocator = By.className("details");
    private final By orderQtyLocator = By.className("qty");


    OrderConfirmPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {
        getWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(orderConfirmationLocator));
    }

    public void check(ProductData productData){
        WebElement contentHookElement = getDriver().findElement(contentHookLocator);
        WebElement messageElement = contentHookElement.findElement(cardTitleLocator);
        String actualMessage = messageElement.getText().trim();
        actualMessage = actualMessage.replace("\uE876", "");
        Assert.assertEquals(actualMessage.toUpperCase(), expectedMessage.toUpperCase());

        WebElement orderItemsElement = getDriver().findElement(orderItemsLocator);
        List<WebElement> items = orderItemsElement.findElements(orderLineLocator);
        
        Assert.assertEquals(items.size(), 1);

        WebElement productNameElement = items.get(0).findElement(orderDetailsLocator);
        WebElement spanElement = productNameElement.findElement(By.xpath("./span"));
        String spanText = spanElement.getText();
        String[] splittedText = spanText.split(" - ");
        Assert.assertEquals(splittedText[0].toUpperCase(), productData.getName().toUpperCase());

        WebElement productQtyElement = items.get(0).findElement(orderQtyLocator);
        WebElement rowElement1 = productQtyElement.findElement(By.className("row"));
        List<WebElement> elements = rowElement1.findElements(By.tagName("div"));

        for (WebElement tempElement:
                elements) {
            String elementText = tempElement.getText();
            if(elementText.contains(",")){
                Assert.assertEquals(DataConverter.parsePriceValue(elementText), productData.getPrice());
            } else {
                Assert.assertEquals(DataConverter.parseStockValue(elementText), 1);
            }
        }
    }

    public void openAllProductsPage(){
        WebElement allProductsBtn = scrollTo(By.className("all-product-link"));
        allProductsBtn.click();
    }
}
