package myprojects.automation.assignment5.pages;

import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class BasketPage extends Page {

    private final By removeBtnLocator = By.className("remove-from-cart");
    private final By basketBtnLocator = By.className("cart-products-count");

    private final By cardItemsLocator = By.className("cart-items");
    private final By cardItemLocator = By.className("cart-item");
    private final By productBodyLocator = By.className("product-line-grid-body");

    private final By checkoutLocator = By.className("checkout");

    BasketPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    public void waitPageLoading() {
        getWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(removeBtnLocator));
    }

    public void checkData(ProductData expectedData){
        WebElement basketBtnElement = getDriver().findElement(basketBtnLocator);
        String basketCount = basketBtnElement.getText();
        Assert.assertEquals(basketCount, "(1)");

        WebElement itemsBlockElement = getDriver().findElement(cardItemsLocator);
        List<WebElement> items = itemsBlockElement.findElements(cardItemLocator);
        
        Assert.assertEquals(items.size(), 1);

        WebElement infoBlockElement = items.get(0).findElement(productBodyLocator);

        WebElement productNameElement = infoBlockElement.findElement(By.tagName("a"));
        String productName = productNameElement.getText();
        Assert.assertEquals(productName.toUpperCase(), expectedData.getName().toUpperCase());
        
        List<WebElement> spans = infoBlockElement.findElements(By.className("value"));
        for (WebElement spanElement :
                spans) {
            String text = spanElement.getText();
            if(text.contains(",")){
                Assert.assertEquals(DataConverter.parsePriceValue(text), expectedData.getPrice());
                break;
            }
        }
    }

    public void makeOrder(){
        WebElement orderElement = scrollTo(checkoutLocator);
        WebElement orderBtn = orderElement.findElement(By.className("btn"));
        orderBtn.click();
    }
}
