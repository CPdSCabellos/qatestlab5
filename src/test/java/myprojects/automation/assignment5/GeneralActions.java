package myprojects.automation.assignment5;


import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.model.UserData;
import myprojects.automation.assignment5.pages.PagesManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private PagesManager pagesManager;

    private ProductData productData;
    private int productIndex;

    GeneralActions(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        pagesManager = new PagesManager(driver, wait);
    }

    public void openMainPage(){
        pagesManager.getMainPage().openPage();
    }

    public void openRandomProduct() {
        pagesManager.getMainPage().showAllProduct();

        pagesManager.getAllProductsPage().waitPageLoading();
        productIndex = pagesManager.getAllProductsPage().showRandomProduct();
    }

    public void orderProduct(){
        pagesManager.getProductPage().waitPageLoading();
        productData = pagesManager.getProductPage().getProductDataInfo();
        pagesManager.getProductPage().addToBasket();

        pagesManager.getConfirmPage().waitPageLoading();
        pagesManager.getConfirmPage().startOrder();

        pagesManager.getBasketPage().waitPageLoading();
        pagesManager.getBasketPage().checkData(productData);
        pagesManager.getBasketPage().makeOrder();
    }

    public void fillOrderInfo(){
        UserData userData = UserData.generate();
        pagesManager.getUserInfoPage().waitPageLoading();
        pagesManager.getUserInfoPage().fillUserInfo(userData);

        pagesManager.getUserInfoPage().fillAddressInfo(userData);
        pagesManager.getUserInfoPage().confirmDelivery();
        pagesManager.getUserInfoPage().checkPayment();
    }

    public void checkOrderConfirm(){
        pagesManager.getOrderConfirmPage().waitPageLoading();
        pagesManager.getOrderConfirmPage().check(productData);
    }

    public void checkQtyChange(){
        pagesManager.getOrderConfirmPage().openAllProductsPage();
        pagesManager.getAllProductsPage().waitPageLoading();
        pagesManager.getAllProductsPage().showProduct(productIndex);

        pagesManager.getProductPage().waitPageLoading();

        ProductData actualData = pagesManager.getProductPage().getProductDataInfo();

        Assert.assertEquals(actualData.getName(), productData.getName());
        Assert.assertEquals(actualData.getPrice(), productData.getPrice());
        Assert.assertEquals(actualData.getQty(), productData.getQty()-1);
    }
}
