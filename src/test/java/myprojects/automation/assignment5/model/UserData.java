package myprojects.automation.assignment5.model;


import net.bytebuddy.utility.RandomString;

import java.util.Random;

/**
 * Hold user information that is used among tests.
 */
public class UserData {
    private String name;
    private String lastName;
    private String email;
    private String address;
    private int postCode;
    private String city;

    public UserData(String name, String lastName, String email, String address, int postCode, String city) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.postCode = postCode;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public int getPostCode() {
        return postCode;
    }

    public String getCity() {
        return city;
    }

    /**
     * @return New user object with random parameters.
     */
    public static UserData generate() {
        Random random = new Random(10000);
        return new UserData(
                "Name " + randomString(),
                "Last "+ randomString(),
                randomString() + "@some.com",
                randomString() + " street",
                random.nextInt(99999),
                randomString() + " city");
    }

    private static String randomString(){
        String alphabet= "abcdefghijklmnopqrstuvwxyz";
        StringBuilder s = new StringBuilder();
        Random random = new Random();
        int randomLen = 1+random.nextInt(5);
        for (int i = 0; i < randomLen; i++) {
            char c = alphabet.charAt(random.nextInt(26));
            s.append(c);
        }
        return s.toString();
    }
}
