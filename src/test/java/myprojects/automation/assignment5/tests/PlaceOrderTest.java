package myprojects.automation.assignment5.tests;

import myprojects.automation.assignment5.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

/*
https://bitbucket.org/qatestlab_automation/lecture-5

Автоматизировать следующий сценарий:
Часть А. Проверка открываемой версии магазина:
1. Перейти на главную страницу магазина.
2. Удостовериться, что пользователю отображается корректная версия сайта (обычная версия сайта с использованием настольного браузера, мобильная версия сайта с использованием мобильного браузера.)

Часть Б. Оформление заказа в магазине:
1. Перейти на главную страницу магазина.
2. Перейти к списку всех товаров воспользовавшись ссылкой «Все товары»
3. Открыть случайный товар из отображаемого списка.
4. Добавить товар в корзину.
5. В сплывающем окне нажать на кнопку «Перейти к оформлению» для перехода в корзину пользователя.
6. Проверить, что в корзине отображается одна позиция, название и цена добавленного товара соответствует значениям, которые отображались на странице товара.
7. Нажать на кнопку «Оформление заказа».
8. Заполнить поля Имя, Фамилия, E-mail (должно быть уникальным) и перейти к следующему шагу оформления заказа.
9. Указать адрес, почтовый индекс, город доставки. Перейти к следующему шагу.
10. Оставить настройки доставки без изменений, перейти к шагу оплаты заказа.
11. Выбрать любой способ оплаты. Отметить опцию «Я ознакомлен(а) и согласен(на) с Условиями обслуживания.» Оформить заказ.
12. В открывшемся окне подтверждения заказа проверить следующие значения:
- Пользователю отображается сообщение «Ваш заказ подтвержден»
- В деталях заказа отображается одна позиция, название и цена товара соответствует значениям, которые отображались на странице товара.
13. Вернуться на страницу товара и проверить изменения количества товара в наличии (в блоке справа, на вкладке «Подробнее о товаре»): количество товара должно уменьшиться на единицу.

Настройте выполнение тестового скрипта таким образом, чтобы при вызове выполнения тестов (mvn test) он выполнился на разных браузерах: Chrome, Firefox, Internet Explorer, на мобильном устройстве Android. Для этого можно в testng.xml воспользоваться возможностью передачи параметров.

Проект должен быть разработан таким образом, чтобы имелась возможность выполнять скрипт используя Selenium Grid. Для настройки тестового грида и проверки работы проекта используйте инструкции по настройке и запуску из лекции (запуск и настройка хаба, нод, подключение ноды с мобильным эмулятором).

Примечания:
А. Логику сценария можно разбить на пару методов @Test.
Б. Описывая локаторы обращайте внимание на то, что дизайн мобильной версии сайта отличается от обычного; подбирайте наиболее подходящие локаторы. Для поиска локаторов в мобильной версии сайта можно воспользоваться возможностями панели разработчика браузера. Например, в браузере Chrome откройте Инструменты разработчика и активируйте эмуляцию мобильного устройства.
В. Некоторые сайты встраивают JS скрипты, которые срабатывают некорректно в процессе выполнения автоматизированного скрипта. В таких случаях необходимо использовать альтернативные способы взаимодействия с элементами интерфейса. К примеру, в предложенном сценарии для автоматизации может некорректно срабатывать очистка полей Количество и Цена на странице редактирования товара. Вместо использования стандартного метода Selenium API clear() для очистки текстового поля можно посылать введение символа Backspace (org.openqa.selenium.Keys.BACK_SPACE) используя метод sendKeys().
 */


public class PlaceOrderTest extends BaseTest {

    @Test
    public void checkSiteVersion() {

        actions.openMainPage();

        By userInfoLocator = By.className("user-info");
        WebElement desktopUserInfoElement = driver.findElement(By.id("_desktop_user_info"));
        WebElement mobileUserInfoElement = driver.findElement(By.id("_mobile_user_info"));
        if(isMobileTesting){
            Assert.assertNotNull(mobileUserInfoElement.findElement(userInfoLocator));
            WebElement infoElement = null;
            try {
                infoElement = desktopUserInfoElement.findElement(userInfoLocator);
            } catch (NoSuchElementException e){
                // we expect it!
            }
            Assert.assertNull(infoElement);
        } else {
            Assert.assertNotNull(desktopUserInfoElement.findElement(userInfoLocator));
            WebElement infoElement = null;
            try{
                infoElement = mobileUserInfoElement.findElement(userInfoLocator);
            } catch (NoSuchElementException e){
                // we expect it!
            }
            Assert.assertNull(infoElement);
        }
    }

    @Test
    public void createNewOrder() {

        actions.openMainPage();

        // open random product

        // save product parameters

        actions.openRandomProduct();

        // add product to Cart and validate product information in the Cart

        actions.orderProduct();

        // proceed to order creation, fill required information

        // place new order and validate order summary
        actions.fillOrderInfo();

        actions.checkOrderConfirm();

        // check updated In Stock value
        actions.checkQtyChange();
    }

}
